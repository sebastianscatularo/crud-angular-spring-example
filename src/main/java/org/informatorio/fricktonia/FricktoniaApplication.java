package org.informatorio.fricktonia;

import org.informatorio.fricktonia.model.comic.Comic;
import org.informatorio.fricktonia.model.comic.ComicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FricktoniaApplication implements CommandLineRunner {
	@Autowired
	private ComicRepository comicRepository;

	public static void main(String[] args) {
		SpringApplication.run(FricktoniaApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		Comic one = new Comic();
		one.setName("Avangers");
		comicRepository.save(one);
	}
}
