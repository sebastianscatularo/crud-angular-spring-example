package org.informatorio.fricktonia.model.comic;

/**
 * Created by informatorio on 03/10/16.
 */

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "comic")
public class Comic implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "url")
    private String url;

    @Column(name = "name")
    private String name;

    @Column(name = "genre")
    private String genre;

    @Column(name = "validation")
    private int validation;

    @Column(name = "universe")
    private String universe;

    @Column(name = "origin")
    private String origin;

    @Column(name = "language")
    private String language;

    @Column(name = "description")
    private String description;
}

