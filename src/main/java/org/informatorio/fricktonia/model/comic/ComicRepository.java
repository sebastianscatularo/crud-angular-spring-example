package org.informatorio.fricktonia.model.comic;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by informatorio on 03/10/16.
 */
public interface ComicRepository extends CrudRepository<Comic, Integer> {
}
