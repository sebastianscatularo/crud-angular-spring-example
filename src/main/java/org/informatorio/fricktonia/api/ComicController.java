package org.informatorio.fricktonia.api;

import org.informatorio.fricktonia.model.comic.Comic;
import org.informatorio.fricktonia.model.comic.ComicRepository;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by informatorio on 03/10/16.
 */
@RestController
public class ComicController {
    private final ComicRepository comicRepostory;

    public ComicController(ComicRepository comicRepository) {
        this.comicRepostory = comicRepository;
    }

    @GetMapping("/users")
    public Principal user(Principal principal) {
        return principal;
    }

    @GetMapping("/comics")
    Collection<Comic> getComics() {
        ArrayList<Comic> comics = new ArrayList<Comic>();
        for (Comic comic : comicRepostory.findAll()) {
            comics.add(comic);
        }
        return comics;
    }

    @DeleteMapping("/comics/{id}")
    public ResponseEntity<Void> deleteComic(@PathVariable int id) {
        comicRepostory.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/comics")
    public ResponseEntity<Void> createComic(@RequestBody Comic comic) {
        comicRepostory.save(comic);
        return ResponseEntity.created(URI.create("/comics")).build();
    }
}
