var angular = angular.module('friktonia', [])
.component('friktonia', {
    templateUrl: "app/components/friktonia/view.html",
    controller: ['$http', function Controller($http) {
        var self = this;
        self.refresh = function() {
            $http({ 
            method: 'GET',
            url: '/comics'
        }).then(
            function Success(response) {
                console.log(response.data);
                self.comics = response.data;
            },
            function Error(error) {

            })
        }
        self.save = function(comic) {
            $http({
                method: 'POST',
                url: '/comics',
                data: JSON.stringify(comic),
                headers: {'Content-Type': 'application/json'}
            });
        }
        self.remove = function(id) {
            $http({
                method: 'DELETE',
                url: '/comics/'+id
            })
        }

        self.refresh();
    }],
    controllerAs: 'friktonia'
});